import React, { Component } from 'react';
import { BrowserRouter ,Route} from 'react-router-dom';

  import BoardsContainer from './trelloComponents/BoardCompo/boardsContainer';

  import Navbar from './trelloComponents/navbar';
  import Lists from './trelloComponents/ListComponent/listContainer'

// import { BrowserRouter ,Route } from 'react-router-dom'

class App extends React.Component {
  render() { 
    return (
      <BrowserRouter basename='/BOARDS'>
      <React.Fragment>
      <Navbar/>
      <Route exact path='/' component={BoardsContainer}/>
      <Route path='/:id' 
      style={{backgroundColor:'black'}} component={Lists}/>
      
      </React.Fragment>
      </BrowserRouter>
    );
  }
}
 
export default App;