import React, { Component } from 'react';
  
import Board from './board';

import CreateBoard from './createBoard';

import * as TrelloApi from '../api'

import {Link} from 'react-router-dom'

class BoardsContainer extends React.Component {
   
    constructor(props)
    {
        super(props)

        this.state={
            boards:[],
            name:''
            
        }
    }

    async CreateBoard()
    {
        const boards= await TrelloApi.getBoards();
         this.setState({
             boards
         })
    }
//  async handleList(id)
//  {
//      const lists=await TrelloApi.getList(id)
//  }


handleChange=(event)=>{
    this.setState({
        name:event.target.value

    })
}
handleSubmit=async (event)=>{
    event.preventDefault()

    const response=await TrelloApi.createBoard(this.state.name)
    // console.log(response)
    this.setState({boards:[response,...this.state.boards]})
}

   componentDidMount()
  {
     this.CreateBoard();
     
  }

 
// onClickList={(id)=>this.handleList(id)
    render() { 
        const {boards}=this.state;
        return (
            <React.Fragment>
            <div  className="d-flex flex-row flex-wrap justify-content-center align-items-center">
            {boards.map(({name,id,prefs})=>(
                <Link to={`${id}`} key={id}>
                <Board  name={name} prefs={prefs} id={id}  key={id} />
                </Link>
                ))}
                <CreateBoard  onHandleChange={this.handleChange} onHandleSubmit={this.handleSubmit}
                name={this.state.name}/>
                </div>
                </React.Fragment>
        );
    }
}
 
export default BoardsContainer;

