import React, { Component } from 'react';
  import'./Boards.css'
const Board=({name,prefs})=>{
    return(
        <div  className=" Board  card m-4 "style={{backgroundColor:'black'}}  >
        <img  className="Board-img" src={prefs.backgroundImage}/>
       <h5 className="card-title" 
      >{name}</h5> 
</div>
    )
}

export default Board;