import React, { Component } from 'react';

import * as TrelloApi from '../api'
import Board from './board';

export default function CreateBoard({onHandleChange,onHandleSubmit,name})
{   
        return (
            <form onSubmit={onHandleSubmit}>
  <div className="mb-3">
    <label htmlFor="board-name" className="form-label">Create Board</label>
    <input value={name}
    type='text'
    className='form-control'
    id='board-name'  onChange={onHandleChange}
    placeholder='Board Name '
    />
  </div>
  <button type="submit" className="btn btn-primary">Submit</button>
</form>
        )
    
}
 
// export default CreateBoard;