import React, { Component } from 'react';

import { Link } from 'react-router-dom';
class Navbar  extends React.Component {
    render() { 
        return (
            <nav className="navbar navbar-dark bg-primary">
            <div className="container-fluid">
            <Link to='/'>
            <a className="navbar-brand" href="#"  style={{fontSize:35,fontWeight:900}}>
                  Trello
             </a>
             </Link>
               </div>
          </nav>
        )
    }
}
 
export default Navbar;