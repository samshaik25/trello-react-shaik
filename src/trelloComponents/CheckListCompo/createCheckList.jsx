
import FeatherIcon from 'feather-icons-react';

export default function CreateCheckList({onHandleChange,onHandleSubmit,name})
{

return(
    <form onSubmit={onHandleSubmit}>
          <div className=' d-flex flex-row'>
          <input  value={name}
          className='form-control'
          type='text'
          onChange={onHandleChange}
          placeholder='Add CheckList'
          />
          <button type='submit' className='btn btn-primary'> +</button>
          </div>
          </form>
          )
          
        }
        // <label htmlFor='board-name' className="form-label">Create list</label>