import React, { Component } from 'react';

import * as TrelloApi from '../api'

import CheckItems from '../CheckItemsCompo/checkItems';

import CreateCheckList from './createCheckList';
import FeatherIcon from 'feather-icons-react';


class CheckList extends React.Component {

  constructor(props)
  {
      super(props)
      this.state={
          check:[],
          checkListName:''
      }
  }

//   Create new list section

handleChange=(event)=>{
    this.setState({
        checkListName:event.target.value

    })
}
handleSubmit=async (event)=>{
    event.preventDefault();
    const response=await TrelloApi.createCheckList(this.state.checkListName,this.props.id)
    this.setState({
        check:[...this.state.check,response ]
    })

}
  
  
  

  
  checkList=async(id)=> 
  {
      console.log('idc',id)
  const check=await TrelloApi.getCheckLIst(id)
  this.setState({check})
console.log(check)
  }
  handleDeleteCheckList= async (id)=>{
      console.log('id',id)
    const check=await TrelloApi.deleteCheckList(id)
    this.setState({check:this.state.check.filter(c=>c.id!=id)})
}


    componentDidMount()
    {
        this.checkList(this.props.id)
    }
    render() { 
        const {check}=this.state
        return (
            <div className='d-flex  flex-column '>
          {check.map((list=>(
              <div className=" d-flex justify-content-between "> 
              <div className="d-flex flex-column p-3" >
              <div className='d-flex justify-content-between border-2'>
              <div className='d-flex'>
              <FeatherIcon icon="list" style={{padding:2}} size='20'/>
              
              <h3>{list.name}</h3>
              </div>
              
              <FeatherIcon icon="trash" onClick={()=>this.handleDeleteCheckList(list.id)}/>

              </div>
              <CheckItems  id={list.id} key={list.id}/>
              </div>
              </div>
              
              )))} 
              <div>
          <CreateCheckList onHandleChange={this.handleChange} onHandleSubmit={this.handleSubmit}
          name={this.state.checkListName}/>
          
          </div>
          </div>
          )
        }
    }

    export default CheckList;
    // <CreateCheckList id={this.props.id} key={this.props.id}
    // <button onClick={()=>this.handleDeleteCheckList(list.id)} className='btn btn-sm btn-danger'>-</button>
// <CreateCheckItems id={list.id} key={list.id} />