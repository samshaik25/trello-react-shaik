import React, { Component } from 'react';

import * as TrelloApi from '../api'

export default function CreateList({onHandleChange,onHandleSubmit,name})
{
        return (
            <div>
            <form onSubmit={onHandleSubmit}>
            <div className='mb-3'>
            <input  value={name}
            className='form-control'
              type='text'
              onChange={onHandleChange}
              placeholder='Create New List'
              
              />
              </div>
              <button type='submit' className='btn btn-primary '> submit</button>
              </form>
              </div>
              );
    
            }
            
            
            // <label htmlFor='board-name' className="form-label">Create list</label>