import React, { Component } from 'react';

import CardContainer from '../CardComponent/cardContainer';

import FeatherIcon from 'feather-icons-react';

import './List.css'

class List extends React.Component {
    render() { 
        let {id,backgroundImage}=this.props
        return (
            <div style={{backgroundImage:backgroundImage}} className=' ListClass m-2' >
            <div  className='  d-flex justify-content-between p-2' >
            <h3> {this.props.name}</h3>
            <FeatherIcon icon="x-circle" size="24" onClick={()=>this.props.onDelete(id)}/>
            </div>
            <CardContainer id={id} />
    
            </div>
            )
        }
    }
 
    export default List;
