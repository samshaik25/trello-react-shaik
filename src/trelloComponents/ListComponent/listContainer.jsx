import React, { Component } from 'react';
 
import * as TrelloApi from '../api'
import List from './list';
import CreateList from './createList';

class Lists extends React.Component {
    constructor(props)
    {
        super(props)

        this.state={
            lists:[],
            name:''
        }
    }
   async getList(id)
   {
       const lists=await TrelloApi.getLists(id)
       this.setState({lists})
      
   }
    deleteList= async (id)=>
   {
    const lists=  await TrelloApi.deleteLists(id)
    this.setState({lists:this.state.lists.filter(list=>list.id!=id)})

   }

   handleChange=(event)=>{
    this.setState({
        name:event.target.value

    })
}
handleSubmit=async (event)=>{
    event.preventDefault()
    const response=await TrelloApi.createList(this.state.name,this.props.match.params.id)
    this.setState({
        lists:[response,...this.state.lists]
    })

}
  componentDidMount()
  {
    let {id}=this.props.match.params;
    this.getList(id)
  }
    render() { 
        const {lists}=this.state
        return (
            <React.Fragment>
            <div className="d-flex flex-row flex-wrap m-3">
            
            {  lists.map(({name,id})=>(
                <List key={id} name={name} id={id}
                backgroundImage={`url(${this.state.backgroundImage})`}
                onDelete={this.deleteList} />
                ) )}
                <div className='m-3'>
                <CreateList  onHandleChange={this.handleChange} onHandleSubmit={this.handleSubmit}
                name={this.state.name}/>
                </div>
                </div>
                </React.Fragment>
                );
            }
        }
        
        export default Lists;
        // <CreateList id={this.props.match.params.id}/>