import axios from 'axios'

const API_KEY= process.env.REACT_APP_API_KEY;
const API_TOKEN= process.env.REACT_APP_API_TOKEN;
// console.log(API_KEY)

export function getBoards(){
    return axios
        .get(`https://api.trello.com/1/members/me/boards?key=${API_KEY}&token=${API_TOKEN}`)
        .then(res=>res.data)
}


export function createBoard(name)
{
    return axios.
    post(`https://api.trello.com/1/boards/?name=${name}&key=${API_KEY}&token=${API_TOKEN}`)
    .then(res=>res.data)
}


export function getLists(id)
{
       return axios
       .get(`https://api.trello.com/1/boards/${id}/lists?&key=${API_KEY}&token=${API_TOKEN}`).
       then(res=>res.data)
       
}

export function createList(name,id)
{
    return axios
    .post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}

export function deleteLists(id)
{
    console.log('h')
    return axios
    .put(`https://api.trello.com/1/lists/${id}/closed?key=${API_KEY}&token=${API_TOKEN}&value=true`)
    .then(res=>res.data)
}


export function getCards(id)
{
    return axios
    .get(`https://api.trello.com/1/lists/${id}/cards?key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}


export function createCard(id,name)
{
    return axios
    .post(`https://api.trello.com/1/cards?idList=${id}&name=${name}&key=${API_KEY}&token=${API_TOKEN}`)
    .then(res=>res.data)
}

export function deleteCard(id)
{
    return axios
    .delete(`https://api.trello.com/1/cards/${id}?key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}


export function getCheckLIst(id)
{
    return axios
    .get(`https://api.trello.com/1/cards/${id}/checklists?key=${API_KEY}&token=${API_TOKEN}`)
    .then(res=>res.data)
}



export function createCheckList(name,id)
{
    return  axios
    .post(`https://api.trello.com/1/cards/${id}/checklists?&name=${name}&key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}


export function deleteCheckList(id)
{
    return axios
    .delete(`https://api.trello.com/1/checklists/${id}?key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}


export function getCheckItems(id)
{
    return axios
    .get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${API_KEY}&token=${API_TOKEN}`)
    .then((res)=>res.data)
}

export function createCheckItem(id,name)
{
    return axios
    .post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&key=${API_KEY}&token=${API_TOKEN}`)
    .then(res=>res.data)
}

export function  deleteCheckItem(listId,itemId)
{
    return axios
    .delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${API_KEY}&token=${API_TOKEN}`)
    .then(res=>res.data)
}