import React from 'react';

import  ReactDom  from 'react-dom';
import './modal.css'

import CheckList from './CheckListCompo/checkList';
import FeatherIcon from 'feather-icons-react';



export default  function Modal({ name,namecheck,id,open,onClose})
{
    if(!open) return null;
    

    //  <div style={over}/>
    return ReactDom.createPortal( 
        <React.Fragment>
        <div style={over}/>
        <div className='modal_style'>
        <div  className='d-flex flex-column'>
        <div className='d-flex flex-row  justify-content-between p-2'>
        <div className=''><h1>{name}</h1></div> 
        <FeatherIcon icon="x-circle" onClick={onClose}/>
        </div>

       <div className='d-flex flex-column justify-content-between  '> <CheckList  id={id}/></div>
        <div className='d-flex flex-column'>
        </div>

        </div>
        
        </div>
        
        </React.Fragment>,
        document.getElementById('portal')
        );
    }
    // <button style={{backgroundColor:'red', width:'10%',alignSelf:'center'}} onClick={onClose}>close</button>