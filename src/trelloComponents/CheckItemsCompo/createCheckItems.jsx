import React, { Component } from 'react';




import * as TrelloApi from '../api'


export default function CreateCheckItems({onHandleChange,onHandleSubmit,name})
{

    
        return (
            <form onSubmit={onHandleSubmit}>
              <div className='mb-3 .bg-secondary.bg-gradient d-flex' >
              <input  value={name}
              className='form-control'
              type='text'
              onChange={onHandleChange}
              placeholder='Add New CheckItem '
              />
              <button type='submit' className='btn btn-primary .bg-secondary.bg-gradient '>Add</button>
              </div>
              </form>
              );
        
}
 
// export default CreateCheckItems;