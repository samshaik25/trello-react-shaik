import React, { Component } from 'react';

import FeatherIcon from 'feather-icons-react';

import * as TrelloApi from '../api'

import CreateCheckItems from './createCheckItems';



class CheckItems extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            checkItemName:[],
            itemName:"",
         }
    }

    // create new checkitem
    handleChange=(event)=>{
        this.setState({
            itemName:event.target.value
    
        })
    }
    handleSubmit=async (event)=>{
        event.preventDefault();
        const response=await TrelloApi.createCheckItem(this.props.id,this.state.itemName)
        this.setState({
            checkItemName: [...this.state.checkItemName,response]
        })
    
    }
    
    
   fetchCheckList= async(id)=>{
       const checkItemName=await TrelloApi.getCheckItems(id)
       this.setState({checkItemName})
   }
   onDelete=async(Listid,ItemId)=>
   {
       await TrelloApi.deleteCheckItem(Listid,ItemId)
    this.setState({
        checkItemName:this.state.checkItemName.filter(item=>item.id!=ItemId)
    })
   }

    componentDidMount()
    {
        console.log('in checkitem ',this.props.id)
        this.fetchCheckList(this.props.id)
    }
    render() { 
        const {checkItemName}=this.state
        console.log(checkItemName+'checkkk')
        return ( <div className='d-flex flex-column p-3'>
             
            {checkItemName.map(item=>(
                <div className='d-flex justify-content-between'>
                <div class="form-check">
  <input class="form-check-input" type="checkbox" value="" style={{marginTop:2}} id="flexCheckDefault"/>
  <label class="form-check-label" for="flexCheckDefault">
    <h4>{item.name}</h4>
  </label>
</div>
<FeatherIcon icon="minus-circle" onClick={()=>this.onDelete(this.props.id,item.id)}/>

</div>
))}

<CreateCheckItems  name={this.state.itemName} onHandleSubmit={this.handleSubmit}  onHandleChange={this.handleChange}/>
            
</div> );
}
}

export default CheckItems;
// <button onClick={()=>this.onDelete(this.props.id,item.id)} className='btn btn-sm btn-danger '>-</button>