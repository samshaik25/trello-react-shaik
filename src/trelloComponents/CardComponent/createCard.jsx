import React, { Component } from 'react';



// class CreateCard extends React.Component {
export default function CreateCard ({onHandleChange,onHandleSubmit,name})
{
     
        return (
            <form onSubmit={onHandleSubmit}>
              <div className='mb-3 .bg-secondary.bg-gradient d-flex' >
              <input  value={name}
              className='form-control'
              type='text'
              onChange={onHandleChange}
              placeholder='Add New Card '
              />
              <button type='submit' className='btn btn-primary .bg-secondary.bg-gradient '>+</button>
              </div>
              </form>
              );
        }
    
    // export default CreateCard;
