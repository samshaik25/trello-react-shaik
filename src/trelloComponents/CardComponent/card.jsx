import { render } from '@testing-library/react';
import React, { useState } from 'react';
import FeatherIcon from 'feather-icons-react';
import Modal from '../modal';

  
    function Card({id,name,onDeleteCard}){

    
// const [modalIsOpen,setModalIsOpen]=useState(false)

  
    const [isOpen,setIsOpen]=useState(false)

  // let {id}=this.props
        return (
            <div className="card m-2"  >
            <div  className=' d-flex justify-content-column'>
            <div className="card-body p-2" onClick={()=>setIsOpen(true)} style={{backgroundColor:'white',color:'black'}}>
            
            {name}
            </div>
            <div className=''>
            <Modal  id={id} name={name} open={isOpen} onClose={()=>setIsOpen(false)} >
            <h1 style={{color:'black'}}>hiii</h1>
            </Modal>
            </div>
            
            
         <button onClick={()=>onDeleteCard(id)} className='btn btn-sm btn-danger'>-</button>
         </div>
         </div>
         );
         
        }
        
        export default Card;
        // <FeatherIcon icon="trash-2" onClick={()=>onDeleteCard(id)} />