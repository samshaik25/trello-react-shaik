import React, { Component } from 'react';
import  Card  from './card';
import CreateCard from './createCard';
import * as TrelloApi from '../api'
import Modall from '../modal';


class CardContainer extends React.Component {
  constructor(props)
  {
      super(props)
      this.state={
          cards:[],
            cardName:''
      }
  }
   
  async getCard(id){

    const cards=await TrelloApi.getCards(id)
    this.setState({
        cards:cards
    })
  }

  
  handleDeleteCard= async (id)=>{

       const cards=await TrelloApi.deleteCard(id)
       this.setState({cards:this.state.cards.filter(card=>card.id!=id)})
  }

  handleChange=(event)=>{
    this.setState({
        cardName:event.target.value

    })
}
handleSubmit=async (event)=>{
    event.preventDefault()
    const response=await TrelloApi.createCard(this.props.id,this.state.cardName)
    this.setState({
        cards:[response,...this.state.cards]
    })

}
    componentDidMount()
    {
        this.getCard(this.props.id)
    }
    render() { 
     const   {cards}=this.state
             console.log(cards)
        return(
            <div>
            { cards.map(({name,id})=>(
                <Card id={id} key={id} name={name}  onDeleteCard={this.handleDeleteCard}/>
                )) }
                <CreateCard name={this.state.cardName} onHandleSubmit={this.handleSubmit} onHandleChange={this.handleChange} />
                </div>
                );
            }
        }
        export default CardContainer;
        // < Modall />